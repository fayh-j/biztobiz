from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from main.utils import *
# Create your models here.


class Admin(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nom de votre compte", unique=True )
    description = models.TextField(max_length=10000, null=True, blank=True, verbose_name="A propos de moi" )
    phone1 = models.IntegerField(verbose_name="Numéro de téléphone whatsapp")
    phone2 = models.IntegerField(null=True, blank=True, verbose_name="Autre numéro de téléphone (Falcutatif)")
    facebook_link = models.CharField(max_length=500,  default="https://...", null=True, blank=True, verbose_name="Lien page facebook")
    twitter_link = models.CharField(max_length=500, default="https://...", null=True, blank=True,  verbose_name="Lien page twitter")
    instagram_link = models.CharField(max_length=500, default="https://...", null=True,  blank=True, verbose_name="Lien page intsagram")
    youtube_link = models.CharField(max_length=500, default="https://...", null=True,  blank=True, verbose_name="Lien page intsagram")
    
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")
    
    
    class Meta:
        verbose_name = "Administration"



class Country(models.Model):
    name = models.CharField(max_length=50, blank=True,  null=True)
    drapeau = models.ImageField(upload_to='flags/', null=True,  blank=True, verbose_name="drapeau")
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")

    class Meta:
        verbose_name = "Pay"
        ordering = ['name']

    def __str__(self):
        return self.name

class City(models.Model):
    name = models.CharField(max_length=50, blank=True,  null=True)
    country = models.ForeignKey(Country, null=True, on_delete = models.PROTECT, verbose_name="Pays")
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")

    class Meta:
        verbose_name = "Ville"
        ordering = ['name']

    def __str__(self):
        return self.name

class Quartier(models.Model):
    name = models.CharField(max_length=50, blank=True,  null=True)
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")

    class Meta:
        verbose_name = "Quartier"
        ordering = ['name']

    def __str__(self):
        return self.name


class Big_category(models.Model):
    name = models.CharField(max_length=50, verbose_name="Nom de la grande catégorie" )
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")
    
    class Meta:
        verbose_name = "Grande Catégorie"

    def __str__(self):
        return self.name


class Categorie(models.Model):
    name = models.CharField(max_length=50, verbose_name="Nom de la catégorie" )
    big_category = models.ForeignKey(Big_category, null=True, on_delete = models.PROTECT, verbose_name="Grande catégorie")
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")
    
    class Meta:
        verbose_name = "Catégorie"

    def __str__(self):
        return self.name




class Tag(models.Model):
    name = models.CharField(max_length=50, verbose_name="Nom du tag" )
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")
    
    class Meta:
        verbose_name = "Tag"

    def __str__(self):
        return self.name


class Grossiste(models.Model):
    user = models.OneToOneField(User, null=True, on_delete = models.CASCADE)
    name = models.CharField(max_length=100, verbose_name="Nom de mon compte", unique=True )
    
    description = models.TextField(max_length=10000, null=True, blank=True, verbose_name="A propos de moi" )
    phone1 = models.IntegerField(verbose_name="Numéro de téléphone whatsapp")
    phone2 = models.IntegerField(null=True, blank=True, verbose_name="Autre numéro de téléphone (Falcutatif)")
    country = models.ForeignKey(Country, null=True, on_delete = models.PROTECT, verbose_name="Pays")
    quartier = models.ForeignKey(Quartier,null=True, blank=True, on_delete = models.PROTECT, verbose_name="Quartier")
    categories = models.ManyToManyField(Categorie, null=True, blank=True)
    tags = models.ManyToManyField(Tag, null=True)
    token = models.CharField(max_length=100, verbose_name="Token" )
    facebook_link = models.CharField(max_length=500, default="https://...", null=True, blank=True, verbose_name="Lien page facebook")
    twitter_link = models.CharField(max_length=500, default="https://...", null=True, blank=True,  verbose_name="Lien page twitter")
    instagram_link = models.CharField(max_length=500, default="https://...", null=True,  blank=True, verbose_name="Lien page instagram")
    city = models.ForeignKey(City, on_delete = models.PROTECT, null=True, blank=True, verbose_name="Ville")

    avatar = models.ImageField(upload_to='avatars/',  verbose_name="Avatar")


    pubChoices = [
        ('true', 'true'),
        ('false', 'false'),
    ]
    pub = models.CharField(
        max_length=5,
        choices=pubChoices,
        default='false',
    )
    token = models.CharField(max_length=100, verbose_name="Token" )
    
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")
    
    class Meta:
        verbose_name = "Grossiste"
    def __str__(self):
        return self.name + ', '+self.user.username 


class Revendeur(models.Model):
    user = models.OneToOneField(User, null=True, on_delete = models.CASCADE)
    name = models.CharField(max_length=100, verbose_name="Nom de votre compte", unique=True )
    description = models.TextField(max_length=10000, null=True, blank=True, verbose_name="A propos de moi" )
    phone1 = models.IntegerField(verbose_name="Numéro de téléphone whatsapp")
    phone2 = models.IntegerField(null=True, blank=True, verbose_name="Autre numéro de téléphone (Falcutatif)")
    country = models.ForeignKey(Country, null=True, on_delete = models.PROTECT, verbose_name="Pays")
    categories = models.ManyToManyField(Categorie,null=True, blank=True)
    quartier = models.ForeignKey(Quartier, null=True, blank=True,  on_delete = models.PROTECT, verbose_name="Quartier")
    grossistes = models.ManyToManyField(Grossiste, null=True, blank=True, verbose_name="Grossistes du Revendeur")
    facebook_link = models.CharField(max_length=500,  default="https://...", null=True, blank=True, verbose_name="Lien page facebook")
    twitter_link = models.CharField(max_length=500, default="https://...", null=True, blank=True,  verbose_name="Lien page twitter")
    instagram_link = models.CharField(max_length=500, default="https://...", null=True,  blank=True, verbose_name="Lien page intsagram")
    avatar = models.ImageField(upload_to='avatars/', verbose_name="Avatar")
    city = models.ForeignKey(City, on_delete = models.PROTECT,  null=True, blank=True, verbose_name="Ville")
    pubChoices = [
        ('true', 'true'),
        ('false', 'false'),
    ]
    pub = models.CharField(
        max_length=5,
        choices=pubChoices,
        default='false',
    )
    token = models.CharField(max_length=100, verbose_name="Token" )
    
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")
    
    
    class Meta:
        verbose_name = "Revendeur"
    def __str__(self):
        return self.name + ' '+ self.user.username 

class Article(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nom de l'article", )
    description = models.TextField(max_length=10000, null=True, blank = True, verbose_name="Description de l'article" )
    tags = models.CharField(max_length=100, null=True, blank = True, verbose_name="Tags" )
    categorie = models.ForeignKey(Categorie, null=True, on_delete = models.PROTECT)
    stock = models.PositiveIntegerField(null=True, default=0, blank=True, verbose_name="Stock disponible")
    stock_price = models.PositiveIntegerField(null=True, default=0, blank=True, verbose_name="Prix du Stock (prix en gros)")
    stock_size = models.PositiveIntegerField(null=True, default=0, blank=True, verbose_name="Nombre d'articles par stock")
    image = models.ImageField(upload_to='images/',  verbose_name="Image de l'article")
    image_mini = models.ImageField(upload_to='images_mini/', default=0, null=True, blank=True, verbose_name="Image miniature de l'article")
    grossiste = models.ForeignKey(Grossiste, null=True, blank=True, on_delete = models.PROTECT, verbose_name="Grossiste")
    pubChoices = [
        ('true', 'true'),
        ('false', 'false'),
    ]
    pub = models.CharField(
        max_length=5,
        choices=pubChoices,
        default='false',
    )
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")
    
    class Meta:
        verbose_name = "Article"

    def __str__(self):
        return self.name


