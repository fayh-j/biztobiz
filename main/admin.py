from django.contrib import admin
from main.models import *
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django import template
from django.template.defaultfilters import stringfilter
 

admin.site.index_title = _('Ziméoo, Administration de site')
admin.site.site_header = _('Administration de mon site Ziméoo')
admin.site.site_title = _('Gestion du contenu')


#admin.site.unregister(User)	
admin.site.register(Country)
admin.site.register(Admin)
admin.site.register(City)
admin.site.register(Quartier)
admin.site.register(Grossiste)
admin.site.register(Revendeur)
admin.site.register(Article)
admin.site.register(Tag)
admin.site.register(Categorie)
admin.site.register(Big_category)
