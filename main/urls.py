from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from main import views
from rest_framework_simplejwt import views as jwt_views


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),


    path('', views.indexView.as_view() ),
    path('search/<str:searchType>/<str:user>/<str:value>', views.searchView.as_view() ),

    path('accounts/<str:type>/<str:value>/<str:token>', views.getView.as_view()),
    path('info/<str:username>', views.getInfoView.as_view()),
    path('user/login/token', jwt_views.TokenObtainPairView.as_view(), name='get_token'),
    path('user/login/token/refresh', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('user/register', views.registerView.as_view() ),

    path('user/register/grossiste/<str:username>/<str:ville>', views.registerGrossisteView.as_view() ),
    path('user/register/revendeur/<str:username>/<str:ville>', views.registerRevendeurView.as_view() ),

    path('article/<int:pk>/<str:token>', views.articleView.as_view() ),


    
]

    