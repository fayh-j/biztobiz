import os, io, json, datetime
from datetime import timedelta
from main.models import *
from main.forms import *


from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.hashers import make_password, check_password
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage, FileSystemStorage
from django.core.exceptions import ObjectDoesNotExist
from django.core import serializers
from django.conf import settings
from django.http import  HttpResponse, HttpResponseRedirect, JsonResponse
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime, timedelta

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import *
from rest_framework.status import (HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_200_OK)
from rest_framework.response import Response

#from mails.mails import sendWelcomMail


class indexView(APIView):
    permission_classes = (AllowAny,)
    def get(self, request):
        error = False
        data = {
                    'city': 'baba',
                }
        registerGrossisteForm = RegisterGrossisteForm(initial=data)
        registerRevendeurForm = RegisterRevendeurForm(initial=data)
        admin = Admin.objects.all()[0]
        #userUpdateForm = UserUpdateForm()
        big_categories_q = Big_category.objects.all()
        qs_json = serializers.serialize('json', big_categories_q)
        big_categories = eval(qs_json)

        for big_categorie in big_categories:
            categories_q = Categorie.objects.filter( big_category = big_categorie['pk'])
            qs_json2 = serializers.serialize('json', categories_q)
            big_categorie['fields']['categories']= eval( qs_json2) 
        #big_categories = json.dumps(big_categories)
        #return HttpResponse(json.dumps(big_categories) , content_type='application/json')
        cities = City.objects.all()
        articles = Article.objects.all()
        revendeurs = Revendeur.objects.all()
        grossistes = Grossiste.objects.all()

        articles_pub = Article.objects.filter( pub='true' )
        revendeurs_pub = Revendeur.objects.filter( pub='true' )
        grossistes_pub = Grossiste.objects.filter( pub='true' )
        return render(request, 'index.html', locals())
        return render(request, 'index2.html', locals())


class searchView(APIView):
    permission_classes = (AllowAny,)
    def get(self, request, searchType, user, value ):
        error = False
        admin = Admin.objects.all()[0]
        registerGrossisteForm = RegisterGrossisteForm()
        registerRevendeurForm = RegisterRevendeurForm()
        if searchType== "category" and user == 'all':
            #loginForm = ConnexionForm()
            #userUpdateForm = UserUpdateForm()
            big_categories_q = Big_category.objects.all()
            qs_json = serializers.serialize('json', big_categories_q)
            big_categories = eval(qs_json)

            for big_categorie in big_categories:
                categories_q = Categorie.objects.filter( big_category = big_categorie['pk'] )
                qs_json2 = serializers.serialize('json', categories_q)
                big_categorie['fields']['categories']= eval( qs_json2) 
            #big_categories = json.dumps(big_categories)
            #return HttpResponse(json.dumps(big_categories) , content_type='application/json')
            cities = City.objects.all()
            articles = Article.objects.filter( categorie__name__contains=value )
            revendeurs = Revendeur.objects.filter( categories__name__contains=value )
            grossistes = Grossiste.objects.filter( categories__name__contains=value )

            articles_pub = Article.objects.filter( pub='true', categorie__name__contains=value )
            revendeurs_pub = Revendeur.objects.filter( pub='true', categories__name__contains=value )
            grossistes_pub = Grossiste.objects.filter( pub='true', categories__name__contains=value )

        elif searchType== "city" and user == 'all':
            #loginForm = ConnexionForm()
            #userUpdateForm = UserUpdateForm()
            big_categories_q = Big_category.objects.all()
            qs_json = serializers.serialize('json', big_categories_q)
            big_categories = eval(qs_json)

            for big_categorie in big_categories:
                categories_q = Categorie.objects.filter( big_category = big_categorie['pk'] )
                qs_json2 = serializers.serialize('json', categories_q)
                big_categorie['fields']['categories']= eval( qs_json2) 
            #big_categories = json.dumps(big_categories)
            #return HttpResponse(json.dumps(big_categories) , content_type='application/json')
            cities = City.objects.all()
            articles = Article.objects.all()
            revendeurs = Revendeur.objects.filter( city__name__contains=value )
            grossistes = Grossiste.objects.filter( city__name__contains=value )

            articles_pub = Article.objects.filter( pub='true' )
            revendeurs_pub = Revendeur.objects.filter( pub='true', city__name__contains=value )
            grossistes_pub = Grossiste.objects.filter( pub='true', city__name__contains=value )
        return render(request, 'index.html', locals())


class getInfoView(APIView):
    permission_classes = (AllowAny,)
    def get(self, request, username):
        try:
            result = {}
            result['type'] ='grossiste'
            result['name']  = Grossiste.objects.get( user = User.objects.get(username=username) ).name
            result['token']  = Grossiste.objects.get( user = User.objects.get(username=username) ).token
            return Response({'result': result}, status=HTTP_200_OK)
        except Exception as e:
            pass
        try:
            result = {}
            result['type'] ='revendeur'
            result['name'] = Revendeur.objects.get( user = User.objects.get(username=username) ).name
            result['token']  = Revendeur.objects.get( user = User.objects.get(username=username) ).token
            return Response({'result': result}, status=HTTP_200_OK)
        except Exception as e:
            pass
        result['type'] ='none'
        result['pk'] = User.objects.get(username=username).pk
        return Response({'result': result}, status=HTTP_200_OK)
        

class getView(APIView):
    permission_classes = (AllowAny,)
    def get(self, request, type, value, token):
        admin = Admin.objects.all()[0]
        error = False
        user=None
        auth = False
        try:
            user = Grossiste.objects.get(token = token)
        except Exception as e:
            try:
                user = Revendeur.objects.get(token = token)
            except Exception as e:
                pass
                #return redirect( '/' )
            
        if user != None: auth = True
        
        
        registerGrossisteForm = RegisterGrossisteForm()
        registerRevendeurForm = RegisterRevendeurForm()
        articleForm = ArticleForm()
        #userUpdateForm = UserUpdateForm()
        big_categories_q = Big_category.objects.all()
        qs_json = serializers.serialize('json', big_categories_q)
        big_categories = eval(qs_json)

        for big_categorie in big_categories:
            categories_q = Categorie.objects.filter( big_category = big_categorie['pk'])
            qs_json2 = serializers.serialize('json', categories_q)
            big_categorie['fields']['categories']= eval( qs_json2) 
        #big_categories = json.dumps(big_categories)
        #return HttpResponse(json.dumps(big_categories) , content_type='application/json')
        cities = City.objects.all()
        revendeurs = Revendeur.objects.all()
        revendeurs_pub = Revendeur.objects.filter( pub='true' )

        if type=='grossiste': 
            articles = Article.objects.filter( grossiste= Grossiste.objects.get( name=value ).pk  )
            articles_pub = Article.objects.filter( pub='true', grossiste= Grossiste.objects.get( name=value ).pk )
            return render(request, 'grossiste.html', locals())
        else:  
            articles = Article.objects.all()
            articles_pub = Article.objects.filter( pub='true' )
            return render(request, 'revendeur.html', locals())


    def post(self, request):
        form = ConnexionForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponse("{0}".format("success"))
        return HttpResponse('error')    


class registerView(APIView):
    permission_classes = (AllowAny,)
    def get(self, request):
        pass

    def post(self, request):

        u = User( username = request.POST.get('username'), email = request.POST.get('username') )
        u.password=make_password("aboripassword")
        u.save()
        return HttpResponse(User.objects.get(username=request.POST.get('username')).pk )


class registerGrossisteView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, username, ville):
        form = RegisterGrossisteForm(request.POST, request.FILES)

        required = 'C' in request.POST
        
        if form.is_valid():
            pk = form.save().pk
            createdGrossiste = Grossiste.objects.get(pk = pk)
            createdGrossiste.user  = User.objects.get(username = username)
            createdGrossiste.city  = City.objects.get(name = 'Cotonou')
            createdGrossiste.save()
            return Response({'pk': pk}, status=HTTP_200_OK)
        return Response({'errors': form.errors}, status=HTTP_400_BAD_REQUEST)

class registerRevendeurView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, username, ville):
        form = RegisterRevendeurForm(request.POST, request.FILES)
        if form.is_valid():
            pk = form.save().pk
            createdRevendeur = Revendeur.objects.get(pk = pk)
            createdRevendeur.user  = User.objects.get(username = username)
            createdRevendeur.city  = City.objects.get(name = 'Cotonou')
            createdRevendeur.save()
            return Response({'pk': pk}, status=HTTP_200_OK)
        return Response({'errors': form.errors}, status=HTTP_400_BAD_REQUEST)


class dashboardView(APIView):
    permission_classes = (AllowAny,)
    def get(self, request):
        user = request.user
        fileForm = FileForm()
        userUpdateForm = UserUpdateForm()
        testiForm = TestiForm()
        #userUpdateForm = UserUpdateForm(request.POST)
        return render(request, 'dashboard.html', locals())


class checkAuthView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        qs =  Whatsapp.objects.filter( user = User.objects.get( username=request.user.username).pk ).count()
        if qs >0: return Response({'status': 'yes'}, status=HTTP_200_OK)
        else :  return Response({'status': 'no'}, status=HTTP_200_OK) 



class logoutView(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        logout(request)
        return Response({'status': 'logout'}, status=HTTP_200_OK)




class articleView(APIView):
    permission_classes = (AllowAny,)
    
    def get(self, request, pk, token):
        admin = Admin.objects.all()[0]
        article = Article.objects.get(pk = pk)
        data = {
                    'name': article.name,
                    'stock': article.stock,
                    'stock_size': article.stock_size,
                    'stock_price': article.stock_price,
                    'categorie': article.categorie,
                    'description': article.description,
                    'tags': article.tags, 
                    'grossiste':article.grossiste,
                }
        updateArticleForm = ArticleForm(initial=data)
        #updateArticleForm = ArticleForm()
        #updateArticleForm = ArticleForm(instance = article)
        
        user = None
        auth = False
        try:
            user = Grossiste.objects.get(token = token)
        except Exception as e:
            try:
                user = Revendeur.objects.get(token = token)
            except Exception as e:
                pass
        
        article = Article.objects.get(pk = pk)
    

        if user != None and article.grossiste.token == token: 
            auth = True

        return render(request, 'single-product.html', locals())

    def delete(self, request, pk, token):
        user = None
        auth = False
        try:
            user = Grossiste.objects.get(token = token)
        except Exception as e:
            try:
                user = Revendeur.objects.get(token = token)
            except Exception as e:
                pass
        article = Article.objects.get(pk = pk)
        if user != None and article.grossiste.token == token: 
            article.delete()
            return HttpResponse('success')
        return HttpResponse('error')
    
    def post(self, request, pk, token):
        form = ArticleForm(request.POST, request.FILES)
        if form.is_valid():
            article_form= form.save()
            try:
                article = Article.objects.get(pk = int(pk) )
                article.grossiste = Grossiste.objects.get(token = token)
                article.name = article_form.name
                article.stock = article_form.stock
                article.stock_size = article_form.stock_size
                article.stock_price = article_form.stock_price
                article.image = article_form.image
                article.image_mini = article_form.image_mini
                article.categorie = article_form.categorie
                article.description = article_form.description
                article.tags = article_form.tags
                article.save()
                article_form.delete()
            except Exception as e:
                article_form.grossiste = Grossiste.objects.get(token = token)
                article_form.save()
            
            return Response({'status': 'success'}, status=HTTP_200_OK) 
        return Response({'status': form.errors}, status=HTTP_200_OK)     

   
        #return redirect( '/accounts/grossiste/'+user.username+'/'+token)

    



class allArticleView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        pass

class allRevendeurView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        pass

class allGrossisteView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        pass

class categoryView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        pass
    def post(self, request):
        pass

    def update(self, request):
        pass


    def delete(self, request):
        pass

class fileUploadView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        fileForm = FileForm()
        #userUpdateForm = UserUpdateForm(request.POST)
        return render(request, 'uploader.html', locals())

    def post(self, request):
        form = FileForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            filename = os.popen('ls media/files/').read().replace('\n','').replace(' ','')
            rename = str(datetime.now() ).replace(' ','')+ filename
            myCmd2 = os.popen('mv  media/files/'+filename+'   static/zimeooUserUpdateFiles/img/'+rename).read()
            imUrl = ImageUrl(url= 'static/zimeooUserUpdateFiles/img/'+rename, user = request.user )
            imUrl.save()
            return HttpResponse("{0}".format("success"))
        return HttpResponse('error')     
            
