from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

# Create your models here.

class Country(models.Model):
    name = models.CharField(max_length=100, blank=True,  null=True)
    drapeau = models.ImageField(upload_to='flags/', null=True,  blank=True, verbose_name="drapeau")
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")

    class Meta:
        verbose_name = "Pay"
        ordering = ['name']

    def __str__(self):
        return self.name

class City(models.Model):
    name = models.CharField(max_length=100, blank=True,  null=True)
    country = models.ForeignKey(Country, null=True, on_delete = models.PROTECT, verbose_name="Pays")
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")

    class Meta:
        verbose_name = "Ville"
        ordering = ['name']

    def __str__(self):
        return self.name

class Quartier(models.Model):
    name = models.CharField(max_length=100, blank=True,  null=True)
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")

    class Meta:
        verbose_name = "Quartier"
        ordering = ['name']

    def __str__(self):
        return self.name


class Big_category(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nom de la grande catégorie" )
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")
    
    class Meta:
        verbose_name = "Grande Catégorie"

    def __str__(self):
        return self.name


class Categorie(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nom de la catégorie" )
    big_category = models.ForeignKey(Big_category, null=True, on_delete = models.PROTECT, verbose_name="Ville")
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")
    
    class Meta:
        verbose_name = "Catégorie"

    def __str__(self):
        return self.name




class Tag(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nom du tag" )
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")
    
    class Meta:
        verbose_name = "Tag"

    def __str__(self):
        return self.name


class Grossiste(models.Model):
	user = models.OneToOneField(User, null=True, on_delete = models.CASCADE)
	phone1 = models.IntegerField(verbose_name="Numéro de téléphone 1")
	phone2 = models.IntegerField(null=True, blank=True, verbose_name="Numéro de téléphone 2")
	city = models.ForeignKey(City, null=True, on_delete = models.PROTECT, verbose_name="Ville")
	quartier = models.ForeignKey(Quartier, null=True, on_delete = models.PROTECT, verbose_name="Quartier")
	categories = models.ManyToManyField(Categorie, null=True)
	tags = models.ManyToManyField(Tag, null=True)
	avatar = models.ImageField(upload_to='avatars/', null=True,  blank=True, verbose_name="Avatar")
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")
    
	class Meta:
		verbose_name = "Grossiste"
	def __str__(self):
		return self.user.username + ', '+self.user.first_name + ' '+ self.user.last_name 


class Revendeur(models.Model):
	user = models.OneToOneField(User, null=True, on_delete = models.CASCADE)
	phone1 = models.IntegerField(verbose_name="Numéro de téléphone 1", null=True, blank=True)
	phone2 = models.IntegerField(null=True, blank=True, verbose_name="Numéro de téléphone 2")
	country = models.ForeignKey(Country, null=True, on_delete = models.PROTECT, verbose_name="Ville")
	city = models.ForeignKey(City, null=True, on_delete = models.PROTECT, verbose_name="Pays")
	avatar = models.ImageField(upload_to='avatars/', null=True,  blank=True, verbose_name="Avatar")
	quartier = models.ForeignKey(Quartier, null=True, on_delete = models.PROTECT, verbose_name="Quartier")
	grossistes = models.ManyToManyField(Grossiste, null=True, blank=True, verbose_name="Grossistes du Revendeur")
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")
    
	
	class Meta:
		verbose_name = "Revendeur"
	def __str__(self):
		return self.user.first_name + ' '+ self.user.last_name 

class Article(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nom de l'article" )
    description = models.CharField(max_length=100, null=True, blank = True, verbose_name="Description de l'article" )
    tags = models.CharField(max_length=100, null=True, blank = True, verbose_name="Tags" )
    categorie = models.ForeignKey(Categorie, null=True, on_delete = models.PROTECT)
    stock = models.IntegerField(null=True, default=0, blank=True, verbose_name="Stock disponible")
    stock_price = models.IntegerField(null=True, default=0, blank=True, verbose_name="Prix du Stock (prix en gros)")
    stock_size = models.IntegerField(null=True, default=0, blank=True, verbose_name="Nombre d'articles par stock")
    image_mini = models.ImageField(upload_to='images_mini/', null=True,  blank=True, verbose_name="Image miniature de l'article")
    image = models.ImageField(upload_to='images/', null=True,  blank=True, verbose_name="Image de l'article")
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")
    
    class Meta:
        verbose_name = "Article"

    def __str__(self):
        return self.name


