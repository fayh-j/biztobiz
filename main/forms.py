from django import forms
from  django.contrib.auth.models import User
from main.models import *


class RegisterForm(forms.Form):
	firstname2 = forms.CharField(label="Nom", max_length=30)
	lastname2 = forms.CharField(label="Prénoms", max_length=30)
	username2 = forms.EmailField(label="Adresse e-mail")
	password2 = forms.CharField(label="Mot de passe", widget=forms.PasswordInput)
	password_conf2 = forms.CharField(label="Confirmer Mot de passe", widget=forms.PasswordInput)

class ConnexionForm(forms.Form):

    username = forms.CharField(label="Votre identifiant", max_length=30)
    #email = forms.EmailField(label="Votre adresse e-mail")

    password = forms.CharField(label="Votre mot de passe", widget=forms.PasswordInput)


class RegisterGrossisteForm(forms.ModelForm):
	class Meta:
		model = Grossiste
		#fields = ('data',)
		exclude = ('created_at', 'user', 'updated_at', 'country', 'city', 'categories', 'tags', 'pub' , 'quartier')
		#fields_required = ['city']

class RegisterRevendeurForm(forms.ModelForm):
	class Meta:
		model = Revendeur
		#fields = ('data',)
		exclude = ('created_at', 'user', 'updated_at', 'grossistes', 'country', 'city', 'categories', 'tags', 'pub' , 'quartier')
		#fields_required = ['city']

class ArticleForm(forms.ModelForm):
	class Meta:
		model = Article
		#fields = ('data',)
		exclude = ('created_at', 'updated_at', 'pub', 'grossiste')
